
FROM registry.centos.org/centos/centos:7

MAINTAINER Karanbir Singh <container-feedback.inb@karan.org>
LABEL License=GPLv2
LABEL Version=0.8.5-2.el7

RUN yum -y install --setopt=tsflags=nodocs epel-release && \
    yum -y install --setopt=tsflags=nodocs mysql-proxy && \ 
    yum -y upgrade && yum clean all


EXPOSE 3306

#maybe we dont need this
EXPOSE 4041

USER 1001

# ofcourse this needs a mysql/mariadb service running in the same
# namespace/project usable over dns as 'mysql'
ENTRYPOINT [ "mysql-proxy", "--proxy-address=0.0.0.0:3306", "--proxy-backend-addresses=mysql:3306" ]